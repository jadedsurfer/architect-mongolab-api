"use strict";
var MongoDB = require('mongolab-provider');

module.exports = function(options, imports, register) {
  var debug = imports.debug("mongolab");
  debug("start");

  debug("register mongolab");
  register(null, {
    mongolab: MongoDB
  });
};
